/*
 * TMainForm - main form (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TMainFormH
#define TMainFormH 1

#include "BitbucketAPI.hpp"
#include <FMX.TabControl.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Gestures.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <REST.Authenticator.OAuth.hpp>
#include <REST.Client.hpp>
#include <IPPeerClient.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Data.Bind.Components.hpp>
#include <System.Actions.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>
#include <memory>

class TMainForm : public TForm {
__published:
    TTabControl *TabControl1;
    TTabItem *TabItem1;
    TToolBar *ToolBar1;
    TSpeedButton *AccountButton;
    TPopup *AccountPopup;
    TPanel *AccountPopupPanel;
    TSpeedButton *LogoutButton;
    TSpeedButton *LoginButton;
    TStatusBar *TestStatusBar;
    TLabel *TestStatusLabel;

    TStyleBook *StyleBook1;

    TGestureManager *GestureManager1;
    TActionList *ActionList1;
    TAction *UserAccount;
    TAction *UserLogout;
    TAction *UserLogin;

    TBitbucketAPI *BitbucketAPI;

    void __fastcall FormShow(TObject *Sender);
    void __fastcall UserAccountUpdate(TObject *Sender);
    void __fastcall UserAccountExecute(TObject *Sender);
    void __fastcall UserLogoutExecute(TObject *Sender);
    void __fastcall UserLoginUpdate(TObject *Sender);
    void __fastcall UserLoginExecute(TObject *Sender);

    void __fastcall BitbucketLoginStateChange(TObject *Sender);
    void __fastcall BitbucketAuthenticate(TObject *Sender,
            const UnicodeString URL);

public:
    __fastcall TMainForm(TComponent *Owner);
    virtual __fastcall ~TMainForm();

protected:
    __property TCustomIniFile *UserIniFile = {read = GetUserIniFile};

    static void __fastcall Log(const UnicodeString Message);
    static void __fastcall Log(const UnicodeString Format, TVarRec *Params,
            const int Params_High);

    TCustomIniFile *__fastcall GetUserIniFile();

private:
    std::shared_ptr<TCriticalSection> FCriticalSection;
    std::shared_ptr<TBitbucketSession> FSession;
    TCustomIniFile *FUserIniFile;
};

extern PACKAGE TMainForm *MainForm;

#endif
