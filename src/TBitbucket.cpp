/*
 * TBitbucket - support for Bitbucket REST API (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "TBitbucket.h"

#include "CppUtils.h"

#include <functional>
#include <memory>
#include <cassert>

#if __cplusplus < 201103L && _WIN32
#include <boost/shared_ptr.hpp>
using boost::shared_ptr;
#endif

#if __APPLE__ && __arm__
// The iOS SDK does not provide 'std::shared_ptr'.
#define shared_ptr auto_ptr
#endif

using namespace std;

namespace {
    /**
     * Implementation of version 1.0 user profiles.
     */
    class TUserProfile1 : public TInterfacedObject, private IUserProfile {
        typedef TInterfacedObject inherited;

    public:
        __property UnicodeString Name = {read = FName, write = FName};
        __property UnicodeString FirstName = {
            read = FFirstName, write = FFirstName};
        __property UnicodeString LastName = {
            read = FLastName, write = FLastName};
        __property UnicodeString AvatarURI = {
            read = FAvatarURI, write = FAvatarURI};

        virtual UnicodeString __stdcall GetName() {
            return FName;
        }

        virtual UnicodeString __stdcall GetDisplayName() {
            return FFirstName + _D(" ") + FLastName;
        }

        virtual UnicodeString __stdcall GetAvatarURI() {
            return FAvatarURI;
        }

        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    private:
        UnicodeString FName;
        UnicodeString FFirstName;
        UnicodeString FLastName;
        UnicodeString FAvatarURI;
    };
}

static const UnicodeString CredentialsSection = _D("Bitbucket Credentials");
static const UnicodeString CredentialsTokenID = _D("Token ID");
static const UnicodeString CredentialsTokenSecret = _D("Token Secret");

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
TBitbucket *Bitbucket;
//---------------------------------------------------------------------------
__fastcall TBitbucket::TBitbucket(TComponent *Owner)
        : TDataModule(Owner) {
    FCriticalSection = make_shared<TCriticalSection>();
}

__fastcall TBitbucket::~TBitbucket() {
}

UnicodeString __fastcall TBitbucket::GetCallbackEndpoint() {
    return FCallbackEndpoint;
}

void __fastcall TBitbucket::SetCallbackEndpoint(const UnicodeString URL) {
    FCallbackEndpoint = URL;
}

UnicodeString __fastcall TBitbucket::GetAuthorizationURL() {
    UnicodeString url = OAuth->AuthenticationEndpoint;
    url += _D("?oauth_token=") + URIEncode(OAuth->AccessToken);
    return url;
}

_di_IUserProfile __fastcall TBitbucket::GetCurrentUserProfile() {
    DelphiLock lock(FCriticalSection);
    if (!FCurrentUserProfile) {
        if (Execute(CurrentUserRequest)) {
            return NULL;
        }

        if (CurrentUserRequest->Response->StatusCode != 200) {
            throw ERESTException(_D("Unexpected HTTP response"));
        }

        auto value = CurrentUserRequest->Response->JSONValue;
        auto object = dynamic_cast<TJSONObject *>(value);
        if (object) {
            auto user = object->GetValue(_D("user"));
            auto userObject = dynamic_cast<TJSONObject *>(user);
            if (userObject) {
                auto profile = new TUserProfile1();
                profile->GetInterface(FCurrentUserProfile);
                assert(FCurrentUserProfile);

                auto name = userObject->GetValue(_D("username"));
                auto firstName = userObject->GetValue(_D("first_name"));
                auto lastName = userObject->GetValue(_D("last_name"));
                auto avatarURI = userObject->GetValue(_D("avatar"));

                profile->Name = dynamic_cast<TJSONString *>(name)->Value();
                profile->FirstName = dynamic_cast<TJSONString *>(firstName)
                        ->Value();
                profile->LastName = dynamic_cast<TJSONString *>(lastName)
                        ->Value();
                profile->AvatarURI = dynamic_cast<TJSONString *>(avatarURI)
                        ->Value();
            }
        }
    }
    return FCurrentUserProfile;
}

// Events.

TNotifyEvent __fastcall TBitbucket::GetOnLoginStateChange() {
    return FOnLoginStateChange;
}

void __fastcall TBitbucket::SetOnLoginStateChange(TNotifyEvent Event) {
    FOnLoginStateChange = Event;
}

void __fastcall TBitbucket::LoginStateChange() {
    if (FOnLoginStateChange) {
        FOnLoginStateChange(this);
    }
}

void __fastcall TBitbucket::SaveLoginState(TCustomIniFile *IniFile) {
    if (OAuth->AccessToken.Length() != 0) {
        IniFile->WriteString(CredentialsSection, CredentialsTokenID,
                OAuth->AccessToken);
        IniFile->WriteString(CredentialsSection, CredentialsTokenSecret,
                OAuth->AccessTokenSecret);
    } else {
        IniFile->DeleteKey(CredentialsSection, CredentialsTokenID);
        IniFile->DeleteKey(CredentialsSection, CredentialsTokenSecret);
    }
}

bool __fastcall TBitbucket::RestoreLoginState(TCustomIniFile *IniFile) {
    DelphiLock lock(FCriticalSection);
    if (FLoginState != TLoginState::LoggedOut) {
        return false;
    }

    auto ID = IniFile->ReadString(CredentialsSection, CredentialsTokenID,
            UnicodeString());
    auto Secret = IniFile->ReadString(CredentialsSection,
            CredentialsTokenSecret, UnicodeString());
    if (ID.Length() != 0 && Secret.Length() != 0) {
        OAuth->AccessToken = ID;
        OAuth->AccessTokenSecret = Secret;
        FLoginState = TLoginState::LoggedIn;
        lock.Reset();

        LoginStateChange();
        return true;
    }
    return false;
}

void __fastcall TBitbucket::Login() {
    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::LoggedOut) {
        return;
    }
    FLoginState = TLoginState::ObtainingRequestToken;
    lock.Reset();

    LoginStateChange();
    TThread::CreateAnonymousThread(CreateProc(&ObtainRequestToken))->Start();
}

void __fastcall TBitbucket::ContinueLogin(const UnicodeString Verifier) {
    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::WaitingForAuthentication) {
        return;
    }
    FLoginState = TLoginState::ObtainingAccessToken;
    lock.Reset();

    LoginStateChange();
    OAuth->VerifierPIN = Verifier;
    TThread::CreateAnonymousThread(CreateProc(&ObtainAccessToken))->Start();
}

void __fastcall TBitbucket::RequestAuthorization() {
    DelphiLock lock(FCriticalSection);
    if (LoginState == TLoginState::ObtainingRequestToken) {
        FLoginState = TLoginState::WaitingForAuthentication;
        lock.Reset();

        LoginStateChange();
    }
}

void __fastcall TBitbucket::FinishLogin() {
    DelphiLock lock(FCriticalSection);
    if (LoginState == TLoginState::ObtainingAccessToken) {
        FLoginState = TLoginState::LoggedIn;
        lock.Reset();

        LoginStateChange();
    }
}

void __fastcall TBitbucket::Logout() {
    DelphiLock lock(FCriticalSection);
    FLoginState = TLoginState::LoggedOut;
    OAuth->AccessToken = UnicodeString();
    OAuth->AccessTokenSecret = UnicodeString();
    lock.Reset();

    FCurrentUserProfile = NULL;
    LoginStateChange();
}

void __fastcall TBitbucket::ObtainRequestToken() {
    try {
        OAuth->AccessToken = UnicodeString();
        OAuth->AccessTokenSecret = UnicodeString();
        OAuth->RequestToken = UnicodeString();
        OAuth->RequestTokenSecret = UnicodeString();
        if (FCallbackEndpoint.Length() == 0) {
            OAuth->CallbackEndpoint = _D("oob");
        } else {
            OAuth->CallbackEndpoint = FCallbackEndpoint;
        }

        shared_ptr<TRESTClient> requestTokenClient(new TRESTClient(NULL));
        requestTokenClient->BaseURL = OAuth->RequestTokenEndpoint;
        requestTokenClient->Authenticator = OAuth;

        // This object will be owned.
        auto request = new TRESTRequest(requestTokenClient.get());
        request->Method = rmPOST;
        request->Accept = _D("application/x-www-form-urlencoded,*/*;q=0.1");
        if (Execute(request)) {
            return;
        }

        if (request->Response->StatusCode != 200) {
            throw ERESTException(_D("Unexpected HTTP response"));
        }

        UnicodeString ID, Secret;
        if (!GetCredentials(request->Response, ID, Secret)) {
            throw ERESTException(_D("Missing credentials in response"));
        }

        /* Note: we must somehow set the temporary credentials as the token
         * credentials to generate a signature right. */
        OAuth->AccessToken = ID;
        OAuth->AccessTokenSecret = Secret;
    } catch (...) {
        TThread::Synchronize(TThread::CurrentThread, Logout);
        throw;
    }
    TThread::Synchronize(TThread::CurrentThread, RequestAuthorization);
}

void __fastcall TBitbucket::ObtainAccessToken() {
    try {
        OAuth->CallbackEndpoint = UnicodeString(); // No longer needed.

        shared_ptr<TRESTClient> accessTokenClient(new TRESTClient(NULL));
        accessTokenClient->BaseURL = OAuth->AccessTokenEndpoint;
        accessTokenClient->Authenticator = OAuth;

        // This object will be owned.
        auto request = new TRESTRequest(accessTokenClient.get());
        request->Method = rmPOST;
        request->Accept = _D("application/x-www-form-urlencoded,*/*;q=0.1");
        // Note: oauth_verifier is not included automatically.
        request->AddParameter(_D("oauth_verifier"), OAuth->VerifierPIN);
        if (Execute(request)) {
            return;
        }

        if (request->Response->StatusCode != 200) {
            throw ERESTException(_D("Unexpected HTTP response"));
        }

        UnicodeString ID, Secret;
        if (!GetCredentials(request->Response, ID, Secret)) {
            throw ERESTException(_D("Missing credentials in response"));
        }

        OAuth->AccessToken = ID;
        OAuth->AccessTokenSecret = Secret;
    } catch (...) {
        TThread::Synchronize(TThread::CurrentThread, Logout);
        throw;
    }
    TThread::Synchronize(TThread::CurrentThread, FinishLogin);
}

bool __fastcall TBitbucket::GetCredentials(TCustomRESTResponse *Response,
        UnicodeString &ID, UnicodeString &Secret) {
    // Note: the REST client library appears to dislike content type
    // 'application/x-www-form-urlencoded'.
    if (SameText(Response->ContentType,
            CONTENTTYPE_APPLICATION_X_WWW_FORM_URLENCODED)) {
        Response->ContentType = CONTENTTYPE_TEXT_PLAIN;
    }

    return Response->GetSimpleValue(_D("oauth_token"), ID)
            && Response->GetSimpleValue(_D("oauth_token_secret"), Secret);
}

bool __fastcall TBitbucket::Execute(TCustomRESTRequest *Request) {
    Request->Execute();
    if (Request->Response->StatusCode == 401) {
        // The token credentials would be invalid.
        if (TThread::CurrentThread->ThreadID != MainThreadID) {
            TThread::Synchronize(TThread::CurrentThread, Logout);
        } else {
            Logout();
        }
        return true;
    }
    return false;
}

// Event handlers.

void __fastcall TBitbucket::OAuthAuthenticate(TCustomRESTRequest *Request,
        bool &Done) {
    if (FLoginState == TLoginState::LoggedOut) {
        // If not logged in, authentication is done.
        Done = true;
    }
}
