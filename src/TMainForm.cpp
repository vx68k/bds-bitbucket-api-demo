/*
 * TMainForm - main form (implementation)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "TMainForm.h"

#include "TBitbucket.h"
#include "CppUtils.h"
#include <client.h>
#include <FMX.Platform.hpp>
#include <System.IOUtils.hpp>
#include <memory>

#if _WIN32
#include <FMX.Platform.Win.hpp>
#include <FMX.Dialogs.hpp>
#include <System.Win.Registry.hpp>
#if __cplusplus < 201103L /* Not C++11 */
#include <boost/shared_ptr.hpp>
using boost::shared_ptr;
#endif
#endif /* _WIN32 */

using namespace std;

namespace {
    /*
     * Function object that updates the Text property of a text control.
     */
    class SetText {
    public:
        __fastcall SetText(TTextControl *TextControl = NULL,
                const UnicodeString Text = _D("")) {
            FTextControl = TextControl;
            FText = Text;
        }

        void __fastcall operator()() {
            if (FTextControl) {
                FTextControl->Text = FText;
            }
        }

    private:
        TTextControl *FTextControl;
        UnicodeString FText;
    };

    /**
     * Returns the interface to a platform service.
     * This is a helper function for 'TPlatformService::GetPlatformService'.
     */
    template<typename I>
    DelphiInterface<I> GetPlatformService() {
        return TPlatformServices::Current->GetPlatformService(__uuidof(I));
    }
}

#pragma package(smart_init)
#pragma link "BitbucketAPI"
#pragma resource "*.fmx"
TMainForm *MainForm;

void __fastcall TMainForm::Log(const UnicodeString Message) {
    TVarRec params[] = {Message};
    Log(_D("%s"), params, 0);
}

void __fastcall TMainForm::Log(const UnicodeString Format, TVarRec *Params,
        const int Params_High) {
    auto logging = GetPlatformService<IFMXLoggingService>();
    if (logging) {
        logging->Log(Format, Params, Params_High);
    }
}

__fastcall TMainForm::TMainForm(TComponent *Owner)
        : TForm(Owner) {
    FCriticalSection = make_shared<TCriticalSection>();

    BitbucketAPI->ClientCredentials->ID = _D("") CLIENT_ID;
    BitbucketAPI->ClientCredentials->Secret = _D("") CLIENT_SECRET;

    FSession.reset(BitbucketAPI->GetSession());

    Bitbucket = new TBitbucket(this);
    Bitbucket->OnLoginStateChange = &BitbucketLoginStateChange;
    Bitbucket->OAuth->ConsumerKey = _D("") CLIENT_ID;
    Bitbucket->OAuth->ConsumerSecret = _D("") CLIENT_SECRET;
    Bitbucket->CallbackEndpoint = _D("") CALLBACK_URI;

#if _WIN32
    typedef HRESULT (WINAPI *REGISTERPROC)(PCWSTR, DWORD);
    HMODULE module = GetModuleHandle(kernel32);
    if (REGISTERPROC proc = reinterpret_cast<REGISTERPROC>
            (GetProcAddress(module, "RegisterApplicationRestart"))) {
        if (FAILED((*proc)(CmdLine, 0))) {
            Log(_D("RegisterApplicationRestart failed"));
        }
    }
#endif
}

__fastcall TMainForm::~TMainForm() {
#if _WIN32
    typedef HRESULT (WINAPI *UNREGISTERPROC)();
    HMODULE module = GetModuleHandle(kernel32);
    if (UNREGISTERPROC proc = reinterpret_cast<UNREGISTERPROC>
            (GetProcAddress(module, "UnregisterApplicationRestart"))) {
        if (FAILED((*proc)())) {
            Log(_D("UnregisterApplicationRestart failed"));
        }
    }
#endif

    Bitbucket = NULL;

    delete FUserIniFile;
}

TCustomIniFile *__fastcall TMainForm::GetUserIniFile() {
    DelphiLock lock(FCriticalSection);
    if (!FUserIniFile) {
#if _WIN32
        FUserIniFile =
                new TRegistryIniFile
                (_D("SOFTWARE\\VX68K.org\\Bitbucket API Demo"));
#else
        // Avoids ambiguity with 'FMX.Objects.TPath'.
        using System::Ioutils::TPath;
        // The path returned 'TPath::GetHomePath' is not writable on iOS.
        auto path = TPath::GetLibraryPath();
        FUserIniFile = new TIniFile(TPath::Combine(path, _D("user.ini")));
#endif
    }
    return FUserIniFile;
}

void __fastcall TMainForm::FormShow(TObject *Sender) {
    Bitbucket->RestoreLoginState(UserIniFile);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserAccountUpdate(TObject *Sender) {
    UserAccount->Enabled = Bitbucket->Authenticated;
    AccountButton->Visible = Bitbucket->Authenticated;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserAccountExecute(TObject *Sender) {
    AccountPopup->IsOpen = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLogoutExecute(TObject *Sender) {
    AccountPopup->IsOpen = false;

    Bitbucket->Logout();
    Bitbucket->SaveLoginState(UserIniFile);
    UserIniFile->UpdateFile();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLoginUpdate(TObject *Sender) {
    typedef TBitbucket::TLoginState TLoginState;
    UserLogin->Enabled = Bitbucket->LoginState == TLoginState::LoggedOut;
    LoginButton->Visible = !Bitbucket->Authenticated;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLoginExecute(TObject *Sender) {
    Bitbucket->Login();
}

void __fastcall TMainForm::BitbucketLoginStateChange(TObject *Sender) {
    if (Bitbucket->Authenticated) {
        Bitbucket->SaveLoginState(UserIniFile);
        UserIniFile->UpdateFile();

        TabControl1->Enabled = true;

        _di_IUserProfile profile = Bitbucket->CurrentUserProfile;
        if (profile) {
            AccountButton->Text = profile->Name;
        }
    } else {
        TabControl1->Enabled = false;

        if (Bitbucket->LoginState == TBitbucket::TLoginState::WaitingForAuthentication) {
            BitbucketAuthenticate(Sender, Bitbucket->AuthorizationURL);
        }
    }
}

void __fastcall TMainForm::BitbucketAuthenticate(TObject *Sender,
        const UnicodeString URL) {
    try {
#if _WIN32
        SHELLEXECUTEINFO info = {};
        info.cbSize = sizeof(SHELLEXECUTEINFO);
        info.hwnd = Fmx::Platform::Win::FormToHWND(this);
        info.lpVerb = _T("open");
        info.lpFile = URL.c_str();
        info.nShow = SW_SHOWNORMAL;
        ShellExecuteEx(&info);

        UnicodeString verifier =
                InputBox(_D("Verification"), _D("&Verification code:"),
                _D("")).Trim();
        if (!verifier.IsEmpty()) {
            Bitbucket->ContinueLogin(verifier);
        } else {
            // We assumes authentication was cancelled.
            Bitbucket->Logout();
        }
#endif
    } catch (...) {
        Bitbucket->Logout();
        throw;
    }
}
