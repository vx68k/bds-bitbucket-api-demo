object Bitbucket: TBitbucket
  OldCreateOrder = False
  Height = 480
  Width = 640
  object OAuth: TOAuth1Authenticator
    OnAuthenticate = OAuthAuthenticate
    AccessTokenEndpoint = 'https://bitbucket.org/api/1.0/oauth/access_token'
    RequestTokenEndpoint = 'https://bitbucket.org/api/1.0/oauth/request_token'
    AuthenticationEndpoint = 'https://bitbucket.org/api/1.0/oauth/authenticate'
    Left = 32
    Top = 32
  end
  object RESTClient: TRESTClient
    Authenticator = OAuth
    Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    BaseURL = 'https://bitbucket.org/api'
    Params = <>
    HandleRedirects = True
    Left = 32
    Top = 96
  end
  object RESTResponse: TRESTResponse
    Left = 32
    Top = 160
  end
  object CurrentUserRequest: TRESTRequest
    Accept = 'application/json,text/plain;q=0.9'
    AcceptCharset = 'UTF-8'
    Client = RESTClient
    Params = <>
    Resource = '1.0/user'
    Response = RESTResponse
    SynchronizedEvents = False
    Left = 32
    Top = 224
  end
  object UsersRequest: TRESTRequest
    Accept = 'application/json,text/plain;q=0.9'
    AcceptCharset = 'UTF-8'
    Client = RESTClient
    Params = <
      item
        Kind = pkURLSEGMENT
        name = 'USER'
        Options = [poAutoCreated]
      end>
    Resource = '2.0/users/{USER}'
    Response = RESTResponse
    SynchronizedEvents = False
    Left = 32
    Top = 288
  end
end
