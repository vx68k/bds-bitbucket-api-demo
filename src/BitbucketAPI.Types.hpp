﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BitbucketAPI.Types.pas' rev: 28.00 (Windows)

#ifndef Bitbucketapi_TypesHPP
#define Bitbucketapi_TypesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Bitbucketapi
{
namespace Types
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCredentials;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCredentials : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::UnicodeString FID;
	System::UnicodeString FSecret;
	void __fastcall SetID(const System::UnicodeString ID);
	void __fastcall SetSecret(const System::UnicodeString Secret);
	
public:
	__fastcall TCredentials(void)/* overload */;
	__fastcall TCredentials(const System::UnicodeString ID, const System::UnicodeString Secret)/* overload */;
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	
__published:
	__property System::UnicodeString ID = {read=FID, write=SetID};
	__property System::UnicodeString Secret = {read=FSecret, write=SetSecret};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCredentials(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Types */
}	/* namespace Bitbucketapi */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BITBUCKETAPI_TYPES)
using namespace Bitbucketapi::Types;
#endif
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BITBUCKETAPI)
using namespace Bitbucketapi;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Bitbucketapi_TypesHPP
