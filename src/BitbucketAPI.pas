{
  BitbucketAPI - definitions for Bitbucket REST API
  Copyright (C) 2014 Kaz Nishimura

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program.  If not, see <http://www.gnu.org/licenses/>.
}

unit BitbucketAPI;

interface

{$SCOPEDENUMS ON}

type
  TRequestAuthorizationEvent = procedure(Sender: TObject;
      const AuthorizationURL: string) of object;

  IUserProfile = interface
    ['{DA880A90-93EC-4763-955C-69CE498538EB}']
    function GetName: string; stdcall;
    function GetDisplayName: string; stdcall;
    function GetAvatarURI: string; stdcall;
    property Name: string read GetName;
    property DisplayName: string read GetDisplayName;
    property AvatarURI: string read GetAvatarURI;
  end;

  IUserProfile2 = interface(IUserProfile)
    ['{D6DF0482-7C4E-4C7A-8FFD-7BDF3E5700C0}']
    function GetKind: string; stdcall;
    function GetWebsiteURI: string; stdcall;
    function GetLocation: string; stdcall;
    property Kind: string read GetKind;
    property WebsiteURI: string read GetWebsiteURI;
    property Location: string read GetLocation;
  end;

implementation

end.
