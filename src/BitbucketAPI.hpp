﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BitbucketAPI.pas' rev: 28.00 (Windows)

#ifndef BitbucketapiHPP
#define BitbucketapiHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <BitbucketAPI.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Bitbucketapi
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TBitbucketSession;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TBitbucketSession : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Bitbucketapi::Types::TCredentials* FClientCredentials;
	Bitbucketapi::Types::TCredentials* FTokenCredentials;
	
public:
	__fastcall TBitbucketSession(Bitbucketapi::Types::TCredentials* const ClientCredentials);
	__property Bitbucketapi::Types::TCredentials* TokenCredentials = {read=FTokenCredentials};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TBitbucketSession(void) { }
	
};

#pragma pack(pop)

__interface IBitbucketSessionFactory;
typedef System::DelphiInterface<IBitbucketSessionFactory> _di_IBitbucketSessionFactory;
__interface  INTERFACE_UUID("{AAB34981-CD69-45AC-8D19-32FE4217480D}") IBitbucketSessionFactory  : public System::IInterface 
{
	
public:
	virtual TBitbucketSession* __stdcall GetSession(Bitbucketapi::Types::TCredentials* ClientCredentials) = 0 ;
};

class DELPHICLASS TBitbucketAPI;
class PASCALIMPLEMENTATION TBitbucketAPI : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	static _di_IBitbucketSessionFactory FSessionFactory;
	Bitbucketapi::Types::TCredentials* FClientCredentials;
	
public:
	static void __fastcall SetSessionFactory(_di_IBitbucketSessionFactory SessionFactory);
	__fastcall virtual TBitbucketAPI(System::Classes::TComponent* Owner);
	__fastcall virtual ~TBitbucketAPI(void);
	Bitbucketapi::Types::TCredentials* __fastcall GetClientCredentials(void);
	TBitbucketSession* __fastcall GetSession(void);
	
__published:
	__property Bitbucketapi::Types::TCredentials* ClientCredentials = {read=FClientCredentials};
};


class DELPHICLASS EBitbucketAPIException;
#pragma pack(push,4)
class PASCALIMPLEMENTATION EBitbucketAPIException : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EBitbucketAPIException(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EBitbucketAPIException(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall EBitbucketAPIException(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall EBitbucketAPIException(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall EBitbucketAPIException(NativeUInt Ident, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall EBitbucketAPIException(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall EBitbucketAPIException(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EBitbucketAPIException(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EBitbucketAPIException(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EBitbucketAPIException(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EBitbucketAPIException(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EBitbucketAPIException(NativeUInt Ident, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EBitbucketAPIException(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Bitbucketapi */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BITBUCKETAPI)
using namespace Bitbucketapi;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BitbucketapiHPP
