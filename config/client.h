/*
 * <client.h> - OAuth 1.0 client credentials
 * Copyright (C) 2014 Kaz Nishimura
 *
 * Copying and distribution of this file, with or without modification, are
 * permitted in any medium without royalty provided the copyright notice and
 * this notice are preserved.  This file is offered as-is, without any
 * warranty.
 */

/*
 * To use your own client credentials, copy this file first into
 * config/PLATFORM, e.g. config/Win32, then remove the following #error
 * directive, and insert yours in the macro definitions below.
 */
#error <client.h> not configured

/* Client identifier, a.k.a. consumer key. */
#define CLIENT_ID ""

/* Client shared-secret, a.k.a. consumer secret. */
#define CLIENT_SECRET ""

/* Callback URI, which MUST be loadable successfully. */
#define CALLBACK_URI "oob"
